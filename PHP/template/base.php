<?php
    session_start();


    if(isset($_POST["add"]))
    {
        if(isset($_SESSION["carrello"]))
        {
            $item_array_id = array_column($_SESSION["carrello"], "item_id");
            if(!in_array($_GET["id"], $item_array_id))
            {
                $count = count($_SESSION["carrello"]);
                $item_array = array(
                    'item_id'			=>	$_GET["id"],
                    'item_name'			=>	$_POST["hidden_name"],
                    'item_price'		=>	$_POST["hidden_price"],
                    'item_quantity'		=>	$_POST["quantity"]
                );
                $_SESSION["carrello"][$count] = $item_array;
            }
            else
            {
                echo '<script>alert("Item Already Added")</script>';
            }
        }
        else
        {
            $item_array = array(
                'item_id'			=>	$_GET["id"],
                'item_name'			=>	$_POST["hidden_name"],
                'item_price'		=>	$_POST["hidden_price"],
                'item_quantity'		=>	$_POST["quantity"]
            );
            $_SESSION["carrello"][0] = $item_array;
        }
    }

if(isset($_GET["action"]))
{
	if($_GET["action"] == "delete")
	{
		foreach($_SESSION["carrello"] as $keys => $values)
		{
			if($values["item_id"] == $_GET["id"])
			{
				unset($_SESSION["carrello"][$keys]);
				echo '<script> alert("Item Removed") </script>';
				echo '<script> window.location="index.php" </script>';
			}
		}
	}
}

?>
<!DOCTYPE html>
<html lang="it">
<head>
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title><?php echo $templateParam["titolo"] ?></title>
    <link rel="stylesheet" type="text/css" href="./css/style.css" />
</head>
<body>
    <header>
        <h1>Shopping</h1>
    </header>
    <main>
    <?php foreach ($templateParam["concerti"] as $concerto): ?>
    <article>
    <form method="post" action="index.php?action=add&id=<?php echo $concerto["id"]; ?>">
                    <div>
                    <img src="<?php echo UPLOAD_DIR.$concerto["imgconcerto"]?>" alt="" />
                    </div>
                    <h2><?php echo $concerto["titolo"]?></h2>
                    <p>A <?php echo $concerto["luogo"]?> il <?php echo $concerto["data"]?> alle ore <?php echo $concerto["ora"]?></p>
                    <p>Prezzo biglietto: <?php echo $concerto["prezzo"]?></p>
                   <footer>
                    <input type="submit" name="add" class = "btn btn-success" value="Compra">
                    <label for="quantity">Quantità</label>
                    <input type="text" name="quantity" value = 1 size="3"/>
                    <input type="hidden" name="hidden_price" value="<?php echo $concerto["prezzo"]?>"/>
                    <input type="hidden" name="hidden_name" value="<?php echo $concerto["titolo"]?>"/>
                    </footer>
                </form>
        </article>
        <?php endforeach ?>

        <article>
            <header>
                <h2>Dettagli</h2>
            </header>
            <div class="table-responsive">
				<table class="table table-bordered">
					<tr>
						<th width="40%">Item Name</th>
						<th width="10%">Quantity</th>
						<th width="20%">Price</th>
						<th width="15%">Total</th>
						<th width="5%">Action</th>
					</tr>

					<?php
					if(!empty($_SESSION["carrello"]))
					{
						$total = 0;
						foreach($_SESSION["carrello"] as $keys => $values)
						{
					?>

					<tr>
						<td><?php echo $values["item_name"]; ?></td>
						<td><?php echo $values["item_quantity"]; ?></td>
						<td>$ <?php echo $values["item_price"]; ?></td>
						<td>$ <?php echo number_format($values["item_quantity"] * $values["item_price"], 2);?></td>
						<td><a href="index.php?action=delete&id=<?php echo $values["item_id"]; ?>"><span class="text-danger">Remove</span></a></td>
					</tr>
					<?php
							$total = $total + ($values["item_quantity"] * $values["item_price"]);
						}
					?>
					<tr>
						<td colspan="3" align="right">Total</td>
						<td align="right">$ <?php echo number_format($total, 2); ?></td>
						<td></td>
					</tr>
					<?php
					}
					?>

				</table>
			</div>
        </article>
</main>
</body>
</html>
