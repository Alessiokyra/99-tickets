<?php 

   class DBHelper{
      private $db;

      public function __construct($host, $user, $password, $db){
         $this->db = new mysqli($host, $user, $password, $db);
         if($this->db->connect_error){
            die("errore");
         }
      }


      public function getConcert(){
         $stmt = $this->db->prepare("SELECT * FROM concerto");
         $stmt->execute();
         $result = $stmt->get_result();

         return $result->fetch_all(MYSQLI_ASSOC);
      }


   }
?>

