-- phpMyAdmin SQL Dump
-- version 4.9.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Creato il: Feb 17, 2020 alle 16:32
-- Versione del server: 10.4.11-MariaDB
-- Versione PHP: 7.2.26

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `progettotecweb`
--

-- --------------------------------------------------------

--
-- Struttura della tabella `artista`
--

CREATE TABLE `artista` (
  `Nome` char(30) NOT NULL,
  `Contatto` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Struttura della tabella `biglietto`
--

CREATE TABLE `biglietto` (
  `id_biglietto` int(11) NOT NULL,
  `id_utente` int(11) NOT NULL,
  `id_concerto` int(11) NOT NULL,
  `pagato` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Struttura della tabella `concerto`
--

CREATE TABLE `concerto` (
  `id` int(11) NOT NULL,
  `titolo` char(50) NOT NULL,
  `luogo` char(200) NOT NULL,
  `data` date NOT NULL,
  `ora` time NOT NULL,
  `nome_artista` char(50) NOT NULL,
  `prezzo` decimal(10,0) NOT NULL,
  `imgconcerto` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dump dei dati per la tabella `concerto`
--

INSERT INTO `concerto` (`id`, `titolo`, `luogo`, `data`, `ora`, `nome_artista`, `prezzo`, `imgconcerto`) VALUES
(1, 'System of a Down', 'Bologna', '2020-04-14', '19:00:00', 'patrik', '30', 'sod.jpg'),
(2, '21 Pilot', 'Ferrara', '2020-08-06', '20:00:00', 'george', '45', '21.jpg'),
(3, 'Green Day', 'Bari', '2020-03-19', '18:00:00', 'Robert', '35', 'greenday.jpg');

-- --------------------------------------------------------

--
-- Struttura della tabella `scaletta`
--

CREATE TABLE `scaletta` (
  `ID` int(11) NOT NULL,
  `nome` char(100) NOT NULL,
  `posizione` int(5) NOT NULL,
  `id_concerto` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Struttura della tabella `utente`
--

CREATE TABLE `utente` (
  `ID` int(11) NOT NULL,
  `Nome` char(50) NOT NULL,
  `Cognome` char(100) NOT NULL,
  `User` char(100) NOT NULL,
  `Password` char(100) NOT NULL,
  `eMail` char(100) NOT NULL,
  `Indirirzzo` char(200) NOT NULL,
  `telefono` int(11) NOT NULL,
  `credito` decimal(10,0) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Indici per le tabelle scaricate
--

--
-- Indici per le tabelle `artista`
--
ALTER TABLE `artista`
  ADD PRIMARY KEY (`Nome`);

--
-- Indici per le tabelle `biglietto`
--
ALTER TABLE `biglietto`
  ADD PRIMARY KEY (`id_biglietto`);

--
-- Indici per le tabelle `concerto`
--
ALTER TABLE `concerto`
  ADD PRIMARY KEY (`id`);

--
-- Indici per le tabelle `scaletta`
--
ALTER TABLE `scaletta`
  ADD PRIMARY KEY (`ID`);

--
-- Indici per le tabelle `utente`
--
ALTER TABLE `utente`
  ADD PRIMARY KEY (`ID`);

--
-- AUTO_INCREMENT per le tabelle scaricate
--

--
-- AUTO_INCREMENT per la tabella `biglietto`
--
ALTER TABLE `biglietto`
  MODIFY `id_biglietto` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT per la tabella `concerto`
--
ALTER TABLE `concerto`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT per la tabella `scaletta`
--
ALTER TABLE `scaletta`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT per la tabella `utente`
--
ALTER TABLE `utente`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
